Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Source: https://github.com/bioperl/bioperl-run/releases

Files: *
Copyright: © 1997–2016 Bioperl Team <bioperl-l@bioperl.org>
 Sendu Bala <bix at sendu.me.uk>
 Jer-Ming Chia <giscjm at nus.edu.sg>
 Rob Edwards <redwards at utmem.edu>
 Mauricio Herrera Cuadra <mauricio at open-bio.org>
 Shawn Hoon <shawnh at fugu-sg.org>
 Donald Jackson <donald.jackson at bms.com>
 Keith James <kdj at sanger.ac.uk>
 Ratnapu Kiran Kumar <kiran at fugu-sg.org>
 Balamurugan Kumarasamy <savikalpa at fugu-sg.org>
 Catherine Letondal <letondal at pasteur.fr>
 Heikki Lehvaslaiho <heikki at ebi.ac.uk>
 Stephen Montgomery <smontgom at bcgsc.bc.ca>
 Brian Osborne <bosborne at alum.mit.edu>
 Tania Oh <gisoht at nus.edu.sg>
 Peter Schattner <schattner at alum.mit.edu>
 Martin Senger <senger at ebi.ac.uk>
 Marc Sohrmann <ms2 at sanger.ac.uk>
 Jason Stajich <jason at bioperl.org>
 Elia Stupka <elia at fugu-sg.org>
 David Vilanova <david.vilanova at urbanet.ch>
 Albert Vilella <avilella at gmail.com>
 Tiequan Zhang <tqzhang1973 at yahoo.com>
 Juguang Xiao <juguang at tll.org.sg>
Comment:
 Everyone can use it! We don't care if you are academic, corporate, or
 government. BioPerl is licensed under the same terms as Perl itself, which
 means it is dually-licensed under either the Artistic or GPL licenses. The Perl
 Artistic License, or the GNU GPL covers all the legalese and what you can and
 can't do with the source code.
 .
 We do appreciate:
 .
  * You letting us know you sell or use a product that uses BioPerl. This helps
    us show people how useful our toolkit is. It also helps us if we seek funding
    from a government source, to identify the utility of the code to many different
    groups of users. Add your project and institution to our BioPerl Users page.
 .
  * If you fix bugs, please let us know about them. Because Bioperl is
    dual-licensed under the GPL or Artistic licenses, you can choose the Artistic
    license, which means that you are not required to submit the code fixes, but in
    the spirit of making a better product we hope you'll contribute back to the
    community any insight or code improvements.
 .
  * Please include the AUTHORS file and ascribe credit to the original BioPerl
    toolkit where appropriate.
 .
  * If you are an academic and you use the software, please cite the article.
    See the BioPerl publications for a list of papers which describe components in
    the toolkit.
 .
 See http://www.bioperl.org/wiki/Licensing_BioPerl
License: Perl

Files: debian/*
Copyright: 2008-2015 Charles Plessy <plessy@debian.org>
           2013-2016 Andreas Tille <tille@debian.org>
License: Perl

License: Perl
 This program is free software; you can redistribute it and/or modify
 it under the terms of either:
 .
 a) the GNU General Public License as published by the Free Software
    Foundation; either version 1, or (at your option) any later
    version, or
 .
 b) the "Artistic License" which comes with Perl.
 .
 On Debian systems, the complete text of the Artistic License can be found in
 ‘/usr/share/common-licenses/Artistic’, and the complete text of the latest
 version of the GNU General Public License version 1 can be found in
 ‘/usr/share/common-licenses/GPL-1’.
